/////////////////////////////////////////////////////////////////////////////
//Fichier Regle.cpp
/////////////////////////////////////////////////////////////////////////////
/**
 * \file Regle.cpp
 * \brief Ce fichier contient une implantation des méthodes de la classe Regle
 * \author Jonathan Plourde
 * \version 0.1
 * \date mai 2017
 *
 */

#include "Regle.h"

namespace tp1
{

/**
 *  \fn Regle::Regle()
 *  \brief Constructeur par défaut
 *  \post Une instance de la classe Regle est initialisée
 */
Regle::Regle(){
}

/**
 *  \fn Regle::~Regle()
 *  \brief Destructeur
 *  \post L'instance de Regle est détruite.
 */
Regle::~Regle(){
	premisses.clear();
	conclusions.clear();
}

/**
 * \fn Regle::Regle(const Regle &r)
 * \brief Constructeur de copie.
 *
 * \pre Il faut qu'il y ait suffisamment de mémoire
 * \post La Regle passée en paramètre est copiée
 * \param[in] r, la Regle à copier
 * \exception bad_alloc s'il n'y a pas assez de mémoire
 */
//Regle::Regle(const Regle &r):premisses(std::list<TypeFait> (0)), conclusions(std::list<TypeFait> (0)){
Regle::Regle(const Regle &r){
	*this = r;
}

/**
 * \fn Regle & Regle::operator =(const Regle &r)
 * \brief Surcharge de l'opérateur =
 *
 * \pre Il y a assez de mémoire pour l'opération
 * \post La Regle passée en paramètre est copiée
 * \param[in] r, la liste à copier
 * \exception bad_alloc si la précondition n'est pas respectée
 */
Regle & Regle::operator =(const Regle &r){
	if(this != &r){
		premisses = r.premisses;
		conclusions = r.conclusions;
	}
	return (*this);
}

/**
 * \fn bool Regle::operator ==(const Regle &r)
 * \brief Surcharge de l'opérateur ==
 *
 * \param[in] r, la Regle à comparer
 * \post VRAI est retourné si la Regle est égale à r, FAUX sinon
 */
bool Regle::operator ==(const Regle &r){
	if(this->premisses == r.premisses && this->conclusions == r.conclusions){
		return true;
	}

	return false;
}

/**
 * \fn bool Regle::operator !=(const Regle &r)
 * \brief Surcharge de l'opérateur !=
 *
 * \param[in] r, la Regle à comparer
 * \post VRAI est retourné si la Regle est égale à r, FAUX sinon
 */
bool Regle::operator !=(const Regle &r){
	if(this->premisses != r.premisses || this->conclusions != r.conclusions){
		return true;
	}

	return false;
}

}

