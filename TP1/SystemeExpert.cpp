/**
 * \file SystemeExpert.cpp
 * \brief Ce fichier contient une implantation des méthodes de la classe SystemeExpert
 * \author Jonathan Plourde
 * \version 0.1
 * \date mai 2017
 *
 */

#include "SystemeExpert.h"

namespace tp1
{

/**
 *  \fn SystemeExpert::SystemeExpert()
 *  \brief Constructeur par défaut
 *  \post Une instance de la classe SystemeExpert est initialisée
 */
SystemeExpert::SystemeExpert(){
}

/**
 *  \fn SystemeExpert::~SystemeExpert()
 *  \brief Destructeur
 *  \post L'instance de SystemeExpert est détruite.
 */
SystemeExpert::~SystemeExpert(){

}

/**
 * \fn SystemeExpert::SystemeExpert(const SystemeExpert &se)
 * \brief Constructeur de copie.
 *
 * \pre Il faut qu'il y ait suffisamment de mémoire
 * \post Le SystemeExpert passée en paramètre est copiée
 * \param[in] se, le SystemeExpert à copier
 * \exception bad_alloc s'il n'y a pas assez de mémoire
 */
SystemeExpert::SystemeExpert(const SystemeExpert &se){
	*this = se;
}


/**
 * \fn SystemeExpert & SystemeExpert::operator =(const SystemeExpert & se)
 * \brief Surcharge de l'opérateur =
 *
 * \pre Il y a assez de mémoire pour l'opération
 * \post Le  SystemeExpert passée en paramètre est copiée
 * \param[in] se, le SystemeExpert à copier
 * \exception bad_alloc si la précondition n'est pas respectée
 */
SystemeExpert & SystemeExpert::operator =(const SystemeExpert &se){
	if(this != &se){
		baseRegles = se.baseRegles;
		baseFaits = se.baseFaits;
	}
	return (*this);
}

/*
 * \fn void SystemeExpert::ajouterRegleSE(const Regle &tr)
 * \brief Ajoute une règle au système expert
 *
 * \param[in] tr, la Regle à ajouter au système expert
 * \post Une Regle est ajouté au système expert
 */
void SystemeExpert::ajouterRegleSE(const Regle &tr){
	if(!baseRegles.appartient(tr))
		baseRegles.ajouter(tr, baseRegles.taille()+1);
}

/*
 * \fn void SystemeExpert::ajouterFaitSE(const TypeFait &tf)
 * \brief Ajoute un Fait au système expert
 *
 * \param[in] tf, le Fait à ajouter au système expert
 * \post Un Fait est ajouté au système expert
 */
void SystemeExpert::ajouterFaitSE(const TypeFait &tf){
	for(const auto &i : baseFaits){
		if(i == tf){
			return;
		}
	}
	baseFaits.push_back(tf);
}

/*
 * \fn void SystemeExpert::chargerSE(std::ifstream & EntreeFichier)
 * \brief Charge un système expert à partir d'un fichier[]
 *
 * \param[in] EntreeFichier, le fichier à lire
 * \post Un système expert à été charger depuis un fichier
 */
void SystemeExpert::chargerSE(std::ifstream & EntreeFichier){
	std::string line;
	Regle ptempRegle;

	if (EntreeFichier.is_open())
	{

		// On parcour le fichier
		while (getline(EntreeFichier,line)){

			if(line == "!>"){
				// Donc, nous avons des conclusion à entrer
				while(getline (EntreeFichier,line) && line != "!%" && line != "!!"){
					ptempRegle.GetConclusions().push_back(line);
				}

				// Après avoir ajouté les conclusions, on ajoute la Regle au système expert
				ajouterRegleSE(ptempRegle);

				// On suprime le jeux de règle
				ptempRegle.GetPremisses().clear();
				ptempRegle.GetConclusions().clear();
			}

			if(line == "!!"){
				while(getline (EntreeFichier,line)){
					ajouterFaitSE(line);
				}
			}else{
				// Si nous avons encore cette" string !%", on en prend une autre
				if(line == "!%"){
					getline (EntreeFichier,line);
				}

				ptempRegle.GetPremisses().push_back(line);
			}
		}
		// On ferme le fichier
		EntreeFichier.close();
	}
}


/*
 * \fn void SystemeExpert::sauvegarderSE(std::ofstream & SortieFichier) const
 * \brief Sauvegarde un système expert à dans un fichier
 *
 * \param[in] SortieFichier, le fichier à écrire
 * \post Un système expert à été sauvegardé dans un fichier
 */
void SystemeExpert::sauvegarderSE(std::ofstream & SortieFichier) const{

	if (SortieFichier.is_open()){
		// Lecture de toutes les Regle dans la liste circulaire sauf la dernière
		for(int i = 1; i <= baseRegles.taille(); i++){
			// On lit toute les prémisses de la règle pour les enregistrer
			for(const auto &j : baseRegles.element(i).GetPremisses()){
				SortieFichier << j << std::endl;
			}
			SortieFichier << "!>" << std::endl;

			// On lit toute les conclusions de la règle pour les enregistrer
			for(const auto &j : baseRegles.element(i).GetConclusions()){
				SortieFichier << j << std::endl;
			}
			if(i != baseRegles.taille()){
				SortieFichier << "!%" << std::endl;
			}
		}

		// On met le délémiteur de fait
		SortieFichier << "!!" << std::endl;

		// On lit toute les Faits du système expert pour les enregistrer
		for(const auto &f : baseFaits){
			SortieFichier << f << std::endl;
		}

		// Fermeture du fichier
		SortieFichier.close();
	}
}

/*
 * \fn void SystemeExpert::chainageAvant(ListeCirculaire<Regle> & er)
 * \brief Chaînage avant pour résoudre le système expert
 *
 * \param[out] er, liste circulaire de règle utilisé
 * \post Un système expert à résolut et er contien les règles utilisé
 */
void SystemeExpert::chainageAvant(ListeCirculaire<Regle> & er){
	ASSERTION(er.taille() == 0);
	size_t prevSize = 0;
	bool findFact = false;
	bool findRegle = true;


	while(prevSize != baseFaits.size()){
		//on enregistre la grandeur de la base de fait
		prevSize = baseFaits.size();

		//on parcour la base de Regle
		for(int i = 1; i <= baseRegles.taille(); i++){
			// Parcour de toute les prémisses de la Regle
			auto j = baseRegles.element(i).GetPremisses().begin();
			while(findRegle && j != baseRegles.element(i).GetPremisses().end()){
				// Parcour de toute les faits de la base
				auto fact = baseFaits.begin();
				while(!findFact && fact != baseFaits.end()){
					if(*j == *fact){
						findFact = true;
					}else{
						findFact = false;
					}
					fact++;
				}

				// Vérification si nous avons trouvé une règle
				if(findFact){
					findRegle = true;
					findFact = false;
				}else{
					findRegle = false;
				}

				j++;
			}

			// Mise à jour du la liste de règle et ajout à la base de fait
			if(findRegle){
				er.ajouter(baseRegles.element(i), er.taille() + 1);
				for(const auto &c : baseRegles.element(i).GetConclusions()){
					ajouterFaitSE(c);
				}
			}

			findFact = false;
			findRegle = true;
		}
	}
}

}
